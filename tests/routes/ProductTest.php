<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class ProductTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @test
     */
    public function index_get_returns_ok_status()
    {
        $this->json('GET', '/')
             ->seeJson(['ok' => true]);
    }

    /**
     * @test
     */
    public function test_store_without_middleware_returns_ok_status()
    {
       $this->post('/', ['title' => 'test', 'remaining_stock' => 5, 'description' => 'blah', 'price' => 500])
             ->seeJson(['ok' => true]);
    }

    /**
     * @test
     */
    public function show_get_returns_ok_status_when_product_exists()
    {
        $product = factory(App\Product::class)->create();
        $id = $product->id;
        $this->json('GET', "/${id}")
             ->seeJson(['ok' => true]);
    }

    /**
     * @test
     */
    public function update_put_returns_ok_status_when_product_exists()
    {
        $product = factory(App\Product::class)->create();
        $id = $product->id;
        $new_title = 'Super Amazing Product!';
        $this->WithoutMiddleware();
        $this->json('PUT', "/${id}", ['title' => $new_title, 'remaining_stock' => 6])
             ->seeJson(['ok' => true]);
        $this->seeInDatabase('products', ['title' => $new_title, 'remaining_stock' => 6]);

    }

    /**
     * @test
     */
    public function destroy_delete_returns_ok_status_when_product_exists()
    {
        $product = factory(App\Product::class)->create();
        $id = $product->id;
        $new_title = 'Super Amazing Product!';
        $this->WithoutMiddleware();
        $this->json('DELETE', "/${id}")
             ->seeJson(['ok' => true]);
    }

}
