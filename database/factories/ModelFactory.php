<?php

$factory->define(App\Product::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->words(3, true),
        'description' => $faker->sentence,
        'remaining_stock' => $faker->numberBetween(0, 30),
	'price' => $faker->numberBetween(100, 5000)
    ];
});
