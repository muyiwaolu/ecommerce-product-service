# ecommerce-product-service

## Requirements

```
* A POSIX Operating System (macOS recommended)
* PHP >= 5.6.4
* OpenSSL PHP Extension
* PDO PHP Extension
* Mbstring PHP Extension
* Composer
* Laravel Lumen
* Laravel Valet
* MySQL / Mariadb
* nginx
```

## Installation

1. Run `composer install` in the directory
2. Run `valet link` in the directory
3. Make sure your database connection details are entered into a `.env` file
  * Keys include `DB_HOST`, `DB_DATABASE`, `DB_USERNAME` and `DB_PASSWORD`
4. Configure the nginx server to use the `public` directory as the root
